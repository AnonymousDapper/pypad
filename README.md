# pypad
A simple cross-platform python editing notepad written in python.

Required modules: PyQt5, pyautogui

**If you use MacOs and can't install pyautogui follow this**

https://stackoverflow.com/questions/35074294/pip3-install-pyautogui-fails-with-error-code-1-mac-os
